package pl.umcs.domain;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.umcs.domain.Exceptions.CannotReserveSeatException;
import pl.umcs.domain.repository.SeanceRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class SeanceTest {

    @Mock
    SeanceRepository seanceRepository;

    @Test
    public void shouldReserveFreeSeat(){
        List<Seat> seats = new ArrayList<>();
        Seat seat = new Seat(1,2);
        seats.add(seat);
        Prices prices = mock(Prices.class);
        Seance seance = new Seance(1, 0, 0, DateTime.now(), seats, prices, seanceRepository);

        Boolean isSeatFreeBeforeReserving = seance.hasSeatFree(seat);
        seance.reserveFreeSeat(seat);
        Boolean isSeatFreeAfterReserving = seance.hasSeatFree(seat);

        assertTrue(isSeatFreeBeforeReserving);
        assertFalse(isSeatFreeAfterReserving);
        verify(seanceRepository, times(1)).put(seance);
    }

    @Test
    public void shouldNotReserveFreeSeat(){
        List<Seat> seats = new ArrayList<>();
        Seat seat1 = new Seat(1,2);
        Seat seat2 = new Seat(2,2);
        seats.add(seat1);
        Prices prices = mock(Prices.class);
        Seance seance = new Seance(0, 0, 0,DateTime.now(), seats, prices, seanceRepository);

        assertThrows(CannotReserveSeatException.class, ()->seance.reserveFreeSeat(seat2));
    }

}
