package pl.umcs.domain;

import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.umcs.domain.repository.ClientRepository;
import pl.umcs.domain.repository.ReservationRepository;
import pl.umcs.domain.repository.SeanceRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ReservationTest {

    @Mock
    ReservationRepository reservationRepository;
    @Mock
    SeanceRepository seanceRepository;
    @Mock
    ClientRepository clientRepository;


    @Test
    public void shouldCancel() {
        Ticket ticket1 = mock(Ticket.class);
        Ticket ticket2 = mock(Ticket.class);
        Ticket ticket3 = mock(Ticket.class);
        Seance seance = mock(Seance.class);

        when(seanceRepository.get(anyLong())).thenReturn(seance);

        List<Ticket> tickets = new ArrayList<>();
        tickets.add(ticket1);
        tickets.add(ticket2);
        tickets.add(ticket3);
        Reservation reservation = new Reservation(1, 0, tickets, 0, reservationRepository, ReservationStatus.NEW, seanceRepository, clientRepository);

        reservation.cancelReservation();
        assertEquals(ReservationStatus.CANCELLED, reservation.getStatus());
    }

    @Test
    public void shouldCalculatePrice(){

        Ticket regularTicket = mock(Ticket.class);
        when(regularTicket.getTicketType()).thenReturn(TicketType.REGULAR);
        Ticket reducedTicket = mock(Ticket.class);
        when(reducedTicket.getTicketType()).thenReturn(TicketType.REDUCED);

        Seance seance = mock(Seance.class);
        when(seance.getPrice(TicketType.REGULAR)).thenReturn(new BigDecimal(22));
        when(seance.getPrice(TicketType.REDUCED)).thenReturn(new BigDecimal(18));

        when(seanceRepository.get(anyLong())).thenReturn(seance);

        List<Ticket> tickets = new ArrayList<>();
        tickets.add(regularTicket);
        tickets.add(reducedTicket);

        Reservation reservation = new Reservation(1,0, tickets, 0, reservationRepository, ReservationStatus.NEW, seanceRepository, clientRepository);

        assertEquals(new BigDecimal(40), reservation.calculatePrice());
    }

    @Test
    public void shouldPayReservation(){
        List<Ticket> tickets = mock(List.class);
        Reservation reservation = new Reservation(1,0, tickets, 0, reservationRepository, ReservationStatus.NEW, seanceRepository, clientRepository);

        reservation.payReservation();

        assertEquals(ReservationStatus.PAID, reservation.getStatus());
    }


    @Test
    public void shouldReserveFreeSeats(){
        Ticket ticket1 = mock(Ticket.class);
        Ticket ticket2 = mock(Ticket.class);
        Ticket ticket3 = mock(Ticket.class);
        Seance seance = mock(Seance.class);

        when(seanceRepository.get(anyLong())).thenReturn(seance);

        List<Ticket> tickets = new ArrayList<>();
        tickets.add(ticket1);
        tickets.add(ticket2);
        tickets.add(ticket3);
        Reservation reservation = new Reservation(1,0, tickets, 0, reservationRepository, ReservationStatus.NEW, seanceRepository, clientRepository);

        Client client = mock(Client.class);
        when(clientRepository.get(anyLong())).thenReturn(client);

        reservation.makeReservation();

        verify(seance, times(3)).reserveFreeSeat(any());

    }

    @Test
    public void shouldAddReservationToClient(){
        List<Ticket> tickets = new ArrayList<>();
        Reservation reservation = new Reservation(1, 0, tickets, 0, reservationRepository, ReservationStatus.NEW, seanceRepository, clientRepository);
        Client client = mock(Client.class);

        when(clientRepository.get(anyLong())).thenReturn(client);

        reservation.makeReservation();

        verify(clientRepository, times(1)).put(any());
    }

    @Test
    public void shouldChangeStatus(){
        List<Ticket> tickets = new ArrayList<>();
        Reservation reservation = new Reservation(1, 0, tickets, 0, reservationRepository, ReservationStatus.NEW, seanceRepository, clientRepository);
        Client client = mock(Client.class);

        when(clientRepository.get(anyLong())).thenReturn(client);

        reservation.makeReservation();

        assertEquals(ReservationStatus.UNPAID, reservation.getStatus());
    }

    @Test
    public void shouldNotChangeStatus(){
        List<Ticket> tickets = new ArrayList<>();
        Reservation reservation = new Reservation(1, 0, tickets, 0, reservationRepository, ReservationStatus.PAID, seanceRepository, clientRepository);
        Client client = mock(Client.class);

        when(clientRepository.get(anyLong())).thenReturn(client);

        reservation.makeReservation();

        assertEquals(ReservationStatus.PAID, reservation.getStatus());
    }

    @Test
    public void shouldReturnTrueWhenOutdated(){
        List<Ticket> tickets = new ArrayList<>();
        Reservation reservation1 = new Reservation(1, 0, tickets, 0, reservationRepository, ReservationStatus.UNPAID, seanceRepository, clientRepository);
        Seance seance = mock(Seance.class);

        when(seanceRepository.get(anyLong())).thenReturn(seance);

        DateTime dateTime1 = DateTime.now().plusMinutes(40);
        DateTime dateTime2 = DateTime.now().plusMinutes(20);
        DateTime dateTime3 = DateTime.now().minusMinutes(20);
        DateTime dateTime4 = DateTime.now().minusMinutes(40);

        when(seance.getShowDate()).thenReturn(dateTime1);
        assertFalse(reservation1.isUnpaidAndOutdated());
        when(seance.getShowDate()).thenReturn(dateTime2);
        assertTrue(reservation1.isUnpaidAndOutdated());
        when(seance.getShowDate()).thenReturn(dateTime3);
        assertTrue(reservation1.isUnpaidAndOutdated());
        when(seance.getShowDate()).thenReturn(dateTime4);
        assertTrue(reservation1.isUnpaidAndOutdated());

    }
}
