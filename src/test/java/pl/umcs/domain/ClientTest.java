package pl.umcs.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.umcs.domain.Exceptions.NotThisClientReservation;
import pl.umcs.domain.repository.ReservationRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ClientTest {

    @Mock
    ReservationRepository reservationRepository;

    @Test
    public void shouldGetReservations(){
        Client client = new Client(0, "email", "nr", "firstName", "lastName", reservationRepository);
        client.addReservation(1L);
        client.addReservation(2L);
        client.addReservation(3L);

        Reservation reservation1 = mock(Reservation.class);
        when(reservation1.getId()).thenReturn(1L);
        when(reservationRepository.get(1L)).thenReturn(reservation1);

        Reservation reservation2 = mock(Reservation.class);
        when(reservation2.getId()).thenReturn(2L);
        when(reservationRepository.get(2L)).thenReturn(reservation2);

        Reservation reservation3 = mock(Reservation.class);
        when(reservation3.getId()).thenReturn(3L);
        when(reservationRepository.get(3L)).thenReturn(reservation3);

        List<Reservation> reservations = client.getReservations();

        assertTrue(reservations.contains(reservation1));
        assertTrue(reservations.contains(reservation2));
        assertTrue(reservations.contains(reservation3));
    }

    @Test
    public void shouldCancelReservation(){
        Reservation reservation1 = mock(Reservation.class);
        when(reservation1.getClientId()).thenReturn(0L);
        when(reservationRepository.get(1L)).thenReturn(reservation1);

        Client client1 = new Client(0,"email1", "nr", "firstName", "lastName", reservationRepository);
        Client client2 = new Client(1,"email2", "nr", "firstName", "lastName", reservationRepository);

        client1.cancelReservation(1L);

        assertThrows(NotThisClientReservation.class, ()->client2.cancelReservation(1L));
        verify(reservation1, times(1)).cancelReservation();

    }
}
