package pl.umcs.adapters.database.inmemory;

import org.junit.jupiter.api.Test;
import pl.umcs.commons.interfaces.Repository;
import pl.umcs.domain.repository.Aggregate;
import pl.umcs.domain.repository.IdGenerator;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class RepositoriesSystemTest {
    @Test
    void createGetAndReplaceAggregates() {
        IdGenerator idGenerator = new IdGeneratorImpl();
        Repository<Agg> aggRepository = new InMemoryRepository<Agg>();

        long id1 = idGenerator.generateIdFor(Agg.class);
        Agg agg1 = new Agg(id1, 10);
        aggRepository.put(agg1);
        long id2 = idGenerator.generateIdFor(Agg.class);
        Agg agg2 = new Agg(id2,20);
        aggRepository.put(agg2);

        Agg agg3 = aggRepository.get(id1);
        agg3.setValue(30);
        aggRepository.put(agg3);

        Collection<Agg> aggs = aggRepository.getAll();
        assertEquals( 2,aggs.size());
        assertTrue(aggs.contains(agg2));
        assertEquals( 30L, aggRepository.get(id1).getValue());
    }

    private class Agg implements Aggregate{
        long id;
        int value;

        public Agg(long id, int value) {
            this.id = id;
            this.value = value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        @Override
        public long getId() {
            return id;
        }
    }
}