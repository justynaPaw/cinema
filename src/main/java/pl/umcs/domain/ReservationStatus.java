package pl.umcs.domain;

public enum ReservationStatus {
    NEW,
    UNPAID,
    PAID,
    CANCELLED
}
