package pl.umcs.domain;

import java.util.Objects;

public class Seat {
    int row;
    int seat;


    public Seat(int row, int seat) {
        this.row = row;
        this.seat = seat;
    }

    public int getRow() {
        return row;
    }

    public int getSeat() {
        return seat;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj.getClass() != this.getClass()){
            return false;
        }
        if(this.row == ((Seat)obj).row && this.seat == ((Seat)obj).seat){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, seat);
    }
}
