package pl.umcs.domain.Exceptions;

public class CannotReserveSeatException extends RuntimeException {
    public CannotReserveSeatException(){
        super("Cannot reserve this seat");
    }
}
