package pl.umcs.domain.Exceptions;

public class NotThisClientReservation extends RuntimeException {
    public NotThisClientReservation() {
        super("Attempt to cancel another client reservation");
    }
}
