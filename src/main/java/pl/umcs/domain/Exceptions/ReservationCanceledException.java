package pl.umcs.domain.Exceptions;

public class ReservationCanceledException extends RuntimeException {
    public ReservationCanceledException() {
        super("This reservation is canceled or outdated");
    }
}
