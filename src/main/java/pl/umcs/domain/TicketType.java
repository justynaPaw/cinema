package pl.umcs.domain;

public enum TicketType {
    REGULAR,
    REDUCED
}
