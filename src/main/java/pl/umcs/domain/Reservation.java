package pl.umcs.domain;

import pl.umcs.domain.Exceptions.ReservationCanceledException;
import pl.umcs.domain.repository.Aggregate;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import pl.umcs.domain.repository.ClientRepository;
import pl.umcs.domain.repository.ReservationRepository;
import pl.umcs.domain.repository.SeanceRepository;

import java.math.BigDecimal;
import java.util.List;

public class Reservation implements Aggregate {
    private long id;
    private long seanceId;
    private List<Ticket> tickets;
    private long clientId;
    private ReservationStatus status;

    private ReservationRepository reservationRepository;
    private SeanceRepository seanceRepository;
    private ClientRepository clientRepository;

    public Reservation(long id, long seanceId, List<Ticket> tickets, long clientId, ReservationRepository reservationRepository,
                       ReservationStatus reservationStatus, SeanceRepository seanceRepository, ClientRepository clientRepository) {
        this.id = id;
        this.seanceId = seanceId;
        this.tickets = tickets;
        this.clientId = clientId;
        this.reservationRepository = reservationRepository;
        this.status = reservationStatus;
        this.seanceRepository = seanceRepository;
        this.clientRepository = clientRepository;
    }

    public long getSeanceId() {
        return seanceId;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public long getClientId() {
        return clientId;
    }

    public ReservationStatus getStatus() {
        return status;
    }

    public void cancelReservation(){
        this.status = ReservationStatus.CANCELLED;
        reservationRepository.put(this);

        Seance seance = seanceRepository.get(seanceId);
        for(Ticket t : tickets) {
            seance.addFreeSeat(t.getSeat());
        }
    }

    public void payReservation(){
        if(this.status == ReservationStatus.CANCELLED){
            throw new ReservationCanceledException();
        }
        this.status = ReservationStatus.PAID;
        reservationRepository.put(this);
    }

    public void makeReservation(){
        Seance seance = seanceRepository.get(seanceId);
        for(Ticket t : tickets) {
            seance.reserveFreeSeat(t.getSeat());
        }

        Client client = clientRepository.get(clientId);
        client.addReservation(id);
        clientRepository.put(client);

        if(this.status == ReservationStatus.NEW) {
            this.status = ReservationStatus.UNPAID;
        }
        reservationRepository.put(this);
    }

    public BigDecimal calculatePrice(){
        BigDecimal totalPrice = new BigDecimal(0);
        Seance seance = seanceRepository.get(seanceId);
        for(Ticket t : tickets){
            TicketType type = t.getTicketType();
            BigDecimal price = seance.getPrice(type);
            totalPrice = totalPrice.add(price);
        }
        return totalPrice;
    }

    public boolean isUnpaidAndOutdated(){
        if(status==ReservationStatus.UNPAID){
            Seance seance = seanceRepository.get(seanceId);
            if(Minutes.minutesBetween(DateTime.now(), seance.getShowDate()).getMinutes() < 30){
                return true;
            }
        }
        return false;
    }

    public void outdatingUpdate(){
        if(isUnpaidAndOutdated()){
            cancelReservation();
        }
    }

    @Override
    public long getId() {
        return id;
    }
}
