package pl.umcs.domain;

import org.joda.time.DateTime;
import pl.umcs.domain.Exceptions.CannotReserveSeatException;
import pl.umcs.domain.repository.Aggregate;
import pl.umcs.domain.repository.SeanceRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Seance implements Aggregate {
    private long id;
    private long movieId;
    private long cinemaId;
    private DateTime showDate;
    private HashSet<Seat> freeSeats;
    private Prices prices;

    private SeanceRepository seanceRepository;

    public Seance(long id, long movieId, long cinemaId, DateTime showDate, List<Seat> freeSeats, Prices prices, SeanceRepository seanceRepository) {
        this.id = id;
        this.movieId = movieId;
        this.cinemaId = cinemaId;
        this.showDate = showDate;
        this.freeSeats = new HashSet<>(freeSeats);
        this.prices = prices;
        this.seanceRepository = seanceRepository;
    }

    @Override
    public long getId() {
        return id;
    }

    public DateTime getShowDate() {
        return showDate;
    }

    public BigDecimal getPrice(TicketType type) {
        return prices.getPrice(type);
    }

    public List<Seat> getFreeSeats(){
        return new ArrayList<>(freeSeats);
    }

    public void reserveFreeSeat(Seat seat){
        if(freeSeats.contains(seat)) {
            freeSeats.remove(seat);
            seanceRepository.put(this);
        }
        else{
            throw new CannotReserveSeatException();
        }
    }

    public void addFreeSeat(Seat seat){
        freeSeats.add(seat);
    }

    public boolean hasSeatFree(Seat seat){
        return freeSeats.contains(seat);
    }


    public static List<Seat> generateEmptySeatsForNewSeance(){
        List<Seat> seats = new ArrayList<>();
        for(int row=1; row<=20; row++){
            for(int seat=1; seat<=30; seat++){
                seats.add(new Seat(row, seat));
            }
        }
        return seats;
    }



}
