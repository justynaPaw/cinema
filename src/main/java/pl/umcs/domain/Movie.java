package pl.umcs.domain;

import pl.umcs.domain.repository.Aggregate;

public class Movie implements Aggregate {
    private long id;
    private String name;

    public Movie(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
