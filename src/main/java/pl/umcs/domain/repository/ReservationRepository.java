package pl.umcs.domain.repository;

import pl.umcs.commons.interfaces.Repository;
import pl.umcs.domain.Reservation;

public interface ReservationRepository extends Repository<Reservation> {
}
