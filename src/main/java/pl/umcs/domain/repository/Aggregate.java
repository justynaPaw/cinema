package pl.umcs.domain.repository;

public interface Aggregate {
    long getId();
}
