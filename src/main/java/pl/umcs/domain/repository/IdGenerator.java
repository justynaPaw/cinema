package pl.umcs.domain.repository;

public interface IdGenerator {
    long generateIdFor(Class t);
}
