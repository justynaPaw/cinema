package pl.umcs.domain.repository;

import pl.umcs.commons.interfaces.Repository;
import pl.umcs.domain.Seance;

public interface SeanceRepository extends Repository<Seance> {
}
