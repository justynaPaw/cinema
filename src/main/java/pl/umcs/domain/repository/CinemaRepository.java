package pl.umcs.domain.repository;

import pl.umcs.commons.interfaces.Repository;
import pl.umcs.domain.Cinema;

public interface CinemaRepository extends Repository<Cinema> {
}
