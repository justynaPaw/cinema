package pl.umcs.domain.repository;

import pl.umcs.commons.interfaces.Repository;
import pl.umcs.domain.Movie;

public interface MovieRepository extends Repository<Movie> {
}
