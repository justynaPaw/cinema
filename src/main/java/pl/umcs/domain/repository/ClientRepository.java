package pl.umcs.domain.repository;

import pl.umcs.commons.interfaces.Repository;
import pl.umcs.domain.Client;

public interface ClientRepository extends Repository<Client> {
    public Client getClientByEmail(String email);
    public boolean existsByEmail(String email);
}
