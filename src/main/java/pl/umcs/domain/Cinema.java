package pl.umcs.domain;

import pl.umcs.domain.repository.Aggregate;

public class Cinema implements Aggregate {
    private long id;
    private String name;

    public Cinema(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public long getId() {
        return id;
    }

    public String getName(){
        return name;
    }
}
