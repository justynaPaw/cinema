package pl.umcs.domain.handlers;

import pl.umcs.commons.interfaces.Handler;
import pl.umcs.domain.Cinema;
import pl.umcs.domain.commands.CreateCinemaCommand;
import pl.umcs.domain.repository.CinemaRepository;
import pl.umcs.domain.repository.IdGenerator;

public class CreateCinemaHandler implements Handler<CreateCinemaCommand, Long> {

    IdGenerator idGenerator;
    CinemaRepository cinemaRepository;

    public CreateCinemaHandler(IdGenerator idGenerator, CinemaRepository cinemaRepository) {
        this.idGenerator = idGenerator;
        this.cinemaRepository = cinemaRepository;
    }

    @Override
    public Long handle(CreateCinemaCommand cmd) {
        Cinema cinema = new Cinema( idGenerator.generateIdFor(Cinema.class), cmd.getCinemaName());
        cinemaRepository.put(cinema);
        return cinema.getId();
    }
}
