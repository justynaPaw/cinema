package pl.umcs.domain.handlers;

import pl.umcs.commons.interfaces.Handler;
import pl.umcs.domain.Reservation;
import pl.umcs.domain.commands.SetReservationPaidCommand;
import pl.umcs.domain.repository.ReservationRepository;

public class SetReservationPaidHandler implements Handler<SetReservationPaidCommand, Boolean> {

    ReservationRepository reservationRepository;

    public SetReservationPaidHandler(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @Override
    public Boolean handle(SetReservationPaidCommand cmd) {
        Reservation reservation = reservationRepository.get(cmd.getReservationId());
        reservation.outdatingUpdate();
        reservation.payReservation();
        return true;
    }
}
