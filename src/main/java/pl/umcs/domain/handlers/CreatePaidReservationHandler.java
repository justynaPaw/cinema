package pl.umcs.domain.handlers;

import pl.umcs.commons.interfaces.Handler;
import pl.umcs.domain.Client;
import pl.umcs.domain.Reservation;
import pl.umcs.domain.ReservationStatus;
import pl.umcs.domain.commands.CreatePaidReservationCommand;
import pl.umcs.domain.repository.ClientRepository;
import pl.umcs.domain.repository.IdGenerator;
import pl.umcs.domain.repository.ReservationRepository;
import pl.umcs.domain.repository.SeanceRepository;

public class CreatePaidReservationHandler implements Handler<CreatePaidReservationCommand, Long> {

    ReservationRepository reservationRepository;
    SeanceRepository seanceRepository;
    ClientRepository clientRepository;
    IdGenerator idGenerator;

    public CreatePaidReservationHandler(ReservationRepository reservationRepository, SeanceRepository seanceRepository, ClientRepository clientRepository, IdGenerator idGenerator) {
        this.reservationRepository = reservationRepository;
        this.seanceRepository = seanceRepository;
        this.clientRepository = clientRepository;
        this.idGenerator = idGenerator;
    }


    @Override
    public Long handle(CreatePaidReservationCommand cmd) {
        Client client;
        if(clientRepository.existsByEmail(cmd.getReservationData().getClientEmail())){
            client = clientRepository.getClientByEmail(cmd.getReservationData().getClientEmail());
        }
        else{
            client = new Client(idGenerator.generateIdFor(Client.class), cmd.getReservationData().getClientEmail(), cmd.getReservationData().getClientPhoneNumber(),
                    cmd.getReservationData().getClientName(), cmd.getReservationData().getClientLastName(), reservationRepository);
            clientRepository.put(client);
        }
        for (Reservation reservation: reservationRepository.getAll()) {
            reservation.outdatingUpdate();
        }
        Reservation reservation = new Reservation(idGenerator.generateIdFor(Reservation.class), cmd.getReservationData().getSeanceId(), cmd.getReservationData().getTickets(),
                client.getId(), reservationRepository, ReservationStatus.PAID, seanceRepository, clientRepository);
        reservation.makeReservation();
        reservationRepository.put(reservation);
        return reservation.getId();
    }
}
