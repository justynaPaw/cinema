package pl.umcs.domain.handlers;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import pl.umcs.commons.interfaces.Handler;
import pl.umcs.domain.Seance;
import pl.umcs.domain.commands.CreateSeanceCommand;
import pl.umcs.domain.repository.IdGenerator;
import pl.umcs.domain.repository.SeanceRepository;

public class CreateSeanceHandler implements Handler<CreateSeanceCommand, Long> {

    SeanceRepository seanceRepository;
    IdGenerator idGenerator;

    public CreateSeanceHandler(SeanceRepository seanceRepository, IdGenerator idGenerator) {
        this.seanceRepository = seanceRepository;
        this.idGenerator = idGenerator;
    }

    @Override
    public Long handle(CreateSeanceCommand cmd) {

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
        String date = cmd.getSeanceDate()+" "+cmd.getSeanceTime();
        Seance seance = new Seance(idGenerator.generateIdFor(Seance.class), cmd.getMovieId(), cmd.getCinemaId(), formatter.parseDateTime(date),
                Seance.generateEmptySeatsForNewSeance(), cmd.getPrices(), seanceRepository);
        seanceRepository.put(seance);
        return seance.getId();
    }
}
