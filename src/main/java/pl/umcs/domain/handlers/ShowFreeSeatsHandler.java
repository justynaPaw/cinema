package pl.umcs.domain.handlers;

import pl.umcs.commons.interfaces.Handler;
import pl.umcs.domain.Reservation;
import pl.umcs.domain.Seat;
import pl.umcs.domain.commands.ShowFreeSeatsCommand;
import pl.umcs.domain.repository.ReservationRepository;
import pl.umcs.domain.repository.SeanceRepository;

import java.util.ArrayList;
import java.util.List;

public class ShowFreeSeatsHandler implements Handler<ShowFreeSeatsCommand, List<Seat>> {

    SeanceRepository seanceRepository;
    ReservationRepository reservationRepository;

    public ShowFreeSeatsHandler(SeanceRepository seanceRepository, ReservationRepository reservationRepository) {
        this.seanceRepository = seanceRepository;
        this.reservationRepository = reservationRepository;
    }

    @Override
    public List<Seat> handle(ShowFreeSeatsCommand cmd) {
        for (Reservation reservation: reservationRepository.getAll()) {
            reservation.outdatingUpdate();
        }
        return seanceRepository.get(cmd.getSeanceId()).getFreeSeats();
    }
}
