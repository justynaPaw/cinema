package pl.umcs.domain.handlers;

import pl.umcs.commons.interfaces.Handler;
import pl.umcs.domain.commands.CancelReservationCommand;
import pl.umcs.domain.repository.ClientRepository;
import pl.umcs.domain.repository.ReservationRepository;

public class CancelReservationHandler implements Handler<CancelReservationCommand, Boolean> {

    ClientRepository clientRepository;

    public CancelReservationHandler(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Boolean handle(CancelReservationCommand cmd) {
        clientRepository.getClientByEmail(cmd.getClientEmail()).cancelReservation(cmd.getReservationId());
        return true;
    }
}
