package pl.umcs.domain.handlers;

import pl.umcs.commons.interfaces.Handler;
import pl.umcs.domain.Movie;
import pl.umcs.domain.commands.CreateMovieCommand;
import pl.umcs.domain.repository.IdGenerator;
import pl.umcs.domain.repository.MovieRepository;

public class CreateMovieHandler implements Handler<CreateMovieCommand, Long> {
    MovieRepository movieRepository;
    IdGenerator idGenerator;

    public CreateMovieHandler(MovieRepository movieRepository, IdGenerator idGenerator) {
        this.movieRepository = movieRepository;
        this.idGenerator = idGenerator;
    }

    @Override
    public Long handle(CreateMovieCommand cmd) {
        Movie movie = new Movie(idGenerator.generateIdFor(Movie.class), cmd.getMovieTitle());
        movieRepository.put(movie);
        return movie.getId();
    }
}
