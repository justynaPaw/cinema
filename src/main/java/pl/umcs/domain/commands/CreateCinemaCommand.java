package pl.umcs.domain.commands;

import pl.umcs.commons.interfaces.Command;

public class CreateCinemaCommand implements Command {
    private String cinemaName;

    public String getCinemaName() {
        return cinemaName;
    }
}
