package pl.umcs.domain.commands;

import org.joda.time.DateTime;
import pl.umcs.commons.interfaces.Command;
import pl.umcs.domain.Prices;

public class CreateSeanceCommand implements Command {
    long movieId;
    long cinemaId;
    String seanceDate;
    String seanceTime;
    Prices prices;

    public long getMovieId() {
        return movieId;
    }

    public long getCinemaId() {
        return cinemaId;
    }

    public String getSeanceDate() {
        return seanceDate;
    }

    public String getSeanceTime() {
        return seanceTime;
    }

    public Prices getPrices() {
        return prices;
    }
}
