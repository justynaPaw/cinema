package pl.umcs.domain.commands;

import pl.umcs.commons.interfaces.Command;

public class CreateMovieCommand implements Command {
    private String movieTitle;

    public String getMovieTitle() {
        return movieTitle;
    }
}
