package pl.umcs.domain.commands;

import pl.umcs.commons.interfaces.Command;

import javax.validation.constraints.*;

public class CancelReservationCommand implements Command {
    @NotNull
    @Pattern.List({@Pattern(regexp = "\\w+@\\w+.\\w+")})
    private String clientEmail;

    private long reservationId;

    public String getClientEmail() {
        return clientEmail;
    }

    public long getReservationId() {
        return reservationId;
    }
}
