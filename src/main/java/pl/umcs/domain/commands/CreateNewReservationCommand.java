package pl.umcs.domain.commands;

import pl.umcs.commons.interfaces.Command;
import pl.umcs.domain.Ticket;

import javax.validation.constraints.*;
import java.util.List;

public class CreateNewReservationCommand implements Command {
    ReservationData reservationData;

    public ReservationData getReservationData() {
        return reservationData;
    }
}
