package pl.umcs.domain.commands;

import pl.umcs.commons.interfaces.Command;

public class ShowFreeSeatsCommand implements Command {
    private long seanceId;

    public long getSeanceId() {
        return seanceId;
    }
}
