package pl.umcs.domain.commands;

import pl.umcs.domain.Ticket;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

public class ReservationData {
    private long seanceId;
    @NotNull
    private List<Ticket> tickets;
    @NotNull
    @Pattern.List({@Pattern(regexp = "\\w+@\\w+.\\w+")})
    private String clientEmail;
    @NotNull
    @Pattern(regexp = "\\d\\d\\d\\s\\d\\d\\d\\s\\d\\d\\d")
    private String clientPhoneNumber;
    @NotNull
    @Pattern(regexp = "\\w+")
    private String clientName;
    @NotNull
    @Pattern(regexp = "\\w+")
    private String clientLastName;

    public long getSeanceId() {
        return seanceId;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public String getClientPhoneNumber() {
        return clientPhoneNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public String getClientLastName() {
        return clientLastName;
    }
}
