package pl.umcs.domain.commands;

import pl.umcs.commons.interfaces.Command;

public class SetReservationPaidCommand implements Command {
    long reservationId;

    public long getReservationId() {
        return reservationId;
    }
}
