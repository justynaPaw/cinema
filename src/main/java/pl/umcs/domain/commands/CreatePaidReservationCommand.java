package pl.umcs.domain.commands;

import pl.umcs.commons.interfaces.Command;

public class CreatePaidReservationCommand implements Command {
    ReservationData reservationData;

    public ReservationData getReservationData() {
        return reservationData;
    }
}
