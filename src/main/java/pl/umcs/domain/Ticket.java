package pl.umcs.domain;

public class Ticket {
    private Seat seat;
    private TicketType ticketType;

    public Ticket(Seat seat, TicketType ticketType) {
        this.seat = seat;
        this.ticketType = ticketType;
    }

    public Seat getSeat() {
        return seat;
    }

    public TicketType getTicketType() {
        return ticketType;
    }
}
