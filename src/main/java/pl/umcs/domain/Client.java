package pl.umcs.domain;

import pl.umcs.domain.Exceptions.NotThisClientReservation;
import pl.umcs.domain.repository.Aggregate;
import pl.umcs.domain.repository.ReservationRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Client implements Aggregate {
    private long id;
    private String email;
    private String  phoneNumber;
    private String firstName;
    private String lastName;
    private List<Long> reservationsIds;

    ReservationRepository reservationRepository;

    public Client(long id, String email, String phoneNumber, String firstName, String lastName, ReservationRepository reservationRepository) {
        this.id = id;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.reservationsIds = new ArrayList<>();
        this.reservationRepository = reservationRepository;
    }

    public void addReservation(Long id){
        reservationsIds.add(id);
    }

    public void addReservations(Collection<Long> collection){
        reservationsIds.addAll(collection);
    }

    public void cancelReservation(long reservationId){
        Reservation reservation = reservationRepository.get(reservationId);
        if(reservation.getClientId() != this.id){
            throw new NotThisClientReservation();
        }
        reservation.cancelReservation();
    }

    public List<Reservation> getReservations(){
        List<Reservation> reservations = new ArrayList<>();
        for(Long l : this.reservationsIds){
            reservations.add(reservationRepository.get(l));
        }
        return reservations;
    }

    @Override
    public long getId() {
        return id;
    }

    public String getEmail(){
        return email;
    }
}
