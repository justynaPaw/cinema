package pl.umcs.domain;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Map;

public class Prices {
    private Map<TicketType, BigDecimal> prices;

    public Prices() {
    }

    public Map<TicketType, BigDecimal> getPrices() {
        return prices;
    }

    public Prices(Map<TicketType, BigDecimal> prices) {
        this.prices = prices;
    }

    public BigDecimal getPrice(TicketType type) {
        return prices.get(type);
    }
}
