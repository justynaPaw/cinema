package pl.umcs.commons.implementation.exceptions;

import pl.umcs.commons.interfaces.Command;

import javax.validation.ConstraintViolation;
import java.util.Set;
import java.util.stream.Collectors;

public class InvalidCommandException extends RuntimeException {

    public <CommandT extends Command> InvalidCommandException(Set<ConstraintViolation<CommandT>> errors) {
        super(errors.stream()
                .map(n -> n.getMessage())
                .collect(Collectors.joining("\n----------\n"))
                );
    }
}
