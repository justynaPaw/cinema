package pl.umcs.commons.implementation.exceptions;

import pl.umcs.commons.interfaces.Command;

public class HandlerNotFoundException extends RuntimeException {
    public HandlerNotFoundException(Command command) {
        super("instance not found for " + command.getClass().getName());
    }
}
