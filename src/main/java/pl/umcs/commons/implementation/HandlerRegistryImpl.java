package pl.umcs.commons.implementation;

import pl.umcs.commons.implementation.exceptions.HandlerNotFoundException;
import pl.umcs.commons.interfaces.Command;
import pl.umcs.commons.interfaces.Handler;
import pl.umcs.commons.interfaces.HandlerRegistry;
import pl.umcs.domain.commands.*;
import pl.umcs.domain.handlers.*;
import pl.umcs.domain.repository.*;

public class HandlerRegistryImpl implements HandlerRegistry {

    IdGenerator idGenerator;
    CinemaRepository cinemaRepository;
    MovieRepository movieRepository;
    SeanceRepository seanceRepository;
    ReservationRepository reservationRepository;
    ClientRepository clientRepository;


    public HandlerRegistryImpl(IdGenerator idGenerator, CinemaRepository cinemaRepository, MovieRepository movieRepository, SeanceRepository seanceRepository,
                               ReservationRepository reservationRepository, ClientRepository clientRepository) {
        this.idGenerator = idGenerator;
        this.cinemaRepository = cinemaRepository;
        this.movieRepository = movieRepository;
        this.seanceRepository = seanceRepository;
        this.reservationRepository = reservationRepository;
        this.clientRepository = clientRepository;
    }

    @Override
    public Handler getHandlerFor(Command command) {
        if(command instanceof CreateCinemaCommand){
            return new CreateCinemaHandler(idGenerator, cinemaRepository);
        }
        if(command instanceof CreateMovieCommand){
            return new CreateMovieHandler(movieRepository, idGenerator);
        }
        if(command instanceof CreateSeanceCommand){
            return new CreateSeanceHandler(seanceRepository, idGenerator);
        }
        if(command instanceof ShowFreeSeatsCommand){
            return new ShowFreeSeatsHandler(seanceRepository, reservationRepository);
        }
        if(command instanceof CreateNewReservationCommand){
            return new CreateNewReservationHandler(reservationRepository, seanceRepository, clientRepository, idGenerator);
        }
        if(command instanceof CreatePaidReservationCommand){
            return new CreatePaidReservationHandler(reservationRepository, seanceRepository, clientRepository, idGenerator);
        }
        if(command instanceof CancelReservationCommand){
            return new CancelReservationHandler(clientRepository);
        }
        if(command instanceof SetReservationPaidCommand){
            return new SetReservationPaidHandler(reservationRepository);
        }
        throw new HandlerNotFoundException(command);
    }
}
