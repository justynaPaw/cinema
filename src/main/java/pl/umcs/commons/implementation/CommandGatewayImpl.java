package pl.umcs.commons.implementation;

import pl.umcs.commons.implementation.exceptions.InvalidCommandException;
import pl.umcs.commons.interfaces.Command;
import pl.umcs.commons.interfaces.CommandGateway;
import pl.umcs.commons.interfaces.Handler;
import pl.umcs.commons.interfaces.HandlerRegistry;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

public class CommandGatewayImpl implements CommandGateway {

    private HandlerRegistry handlerRegistry;
    private Validator validator;

    public CommandGatewayImpl(HandlerRegistry handlerRegistry, Validator validator) {
        this.handlerRegistry = handlerRegistry;
        this.validator = validator;
    }

    @Override
    public <ReturnT, CommandT extends Command> ReturnT execute(CommandT command) {
        validate(command);
        Handler<CommandT, ReturnT> handler = handlerRegistry.getHandlerFor(command);
        return handler.handle(command);
    }

    private <CommandT extends Command> void validate(CommandT command) {
        Set<ConstraintViolation<CommandT>> errors = validator.validate(command);
        if(!errors.isEmpty())
            throw new InvalidCommandException(errors);
    }
}
