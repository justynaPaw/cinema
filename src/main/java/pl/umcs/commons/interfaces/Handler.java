package pl.umcs.commons.interfaces;

public interface Handler<CommandT extends Command, ReturnT> {
    ReturnT handle(CommandT cmd);
}
