package pl.umcs.commons.interfaces;

import java.util.Collection;

public interface Repository<AggregateT> {

    void put(AggregateT agg);

    AggregateT get(long id);

    Collection<AggregateT> getAll();
}
