package pl.umcs.commons.interfaces;

public interface HandlerRegistry {
    Handler getHandlerFor(Command command);

}
