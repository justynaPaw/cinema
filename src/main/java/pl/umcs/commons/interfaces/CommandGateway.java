package pl.umcs.commons.interfaces;

public interface CommandGateway {
    <ReturnT, CommandT extends Command> ReturnT execute (CommandT command);
}
