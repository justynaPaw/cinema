package pl.umcs.adapters.database.inmemory.repositories;

import pl.umcs.adapters.database.inmemory.InMemoryRepository;
import pl.umcs.domain.Seance;
import pl.umcs.domain.repository.SeanceRepository;

public class InMemorySeanceRepository extends InMemoryRepository<Seance> implements SeanceRepository {
}
