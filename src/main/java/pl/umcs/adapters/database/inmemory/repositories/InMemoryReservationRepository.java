package pl.umcs.adapters.database.inmemory.repositories;

import pl.umcs.adapters.database.inmemory.InMemoryRepository;
import pl.umcs.domain.Reservation;
import pl.umcs.domain.repository.ReservationRepository;

public class InMemoryReservationRepository extends InMemoryRepository<Reservation> implements  ReservationRepository{
}
