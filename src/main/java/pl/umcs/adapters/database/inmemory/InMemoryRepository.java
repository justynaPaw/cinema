package pl.umcs.adapters.database.inmemory;

import pl.umcs.commons.interfaces.Repository;
import pl.umcs.domain.repository.Aggregate;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class InMemoryRepository<AggregateT extends Aggregate> implements Repository<AggregateT> {

    protected Map<Long, AggregateT> data = new HashMap<Long, AggregateT>();

    @Override
    public void put(AggregateT agg) {
        data.put(agg.getId(), agg);
    }

    @Override
    public AggregateT get(long id) {
        return data.get(id);
    }

    @Override
    public Collection<AggregateT> getAll() {
        return data.values();
    }
}
