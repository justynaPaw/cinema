package pl.umcs.adapters.database.inmemory.repositories;

import pl.umcs.adapters.database.inmemory.InMemoryRepository;
import pl.umcs.domain.Movie;
import pl.umcs.domain.repository.MovieRepository;

public class InMemoryMovieRepository extends InMemoryRepository<Movie> implements MovieRepository {
}
