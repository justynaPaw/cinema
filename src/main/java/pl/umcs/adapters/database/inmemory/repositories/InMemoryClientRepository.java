package pl.umcs.adapters.database.inmemory.repositories;

import pl.umcs.adapters.database.inmemory.InMemoryRepository;
import pl.umcs.domain.Client;
import pl.umcs.domain.repository.ClientRepository;

import java.util.Optional;

public class InMemoryClientRepository extends InMemoryRepository<Client> implements ClientRepository {
    @Override
    public Client getClientByEmail(String email) {
        Optional<Client> clientOpt = data.values()
                .stream()
                .filter(client -> client.getEmail().equals(email))
                .findFirst();
        return clientOpt.orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public boolean existsByEmail(String email) {
        Optional<Client> clientOpt = data.values()
                .stream()
                .filter(client -> client.getEmail().equals(email))
                .findFirst();
        return clientOpt.isPresent();
    }
}
