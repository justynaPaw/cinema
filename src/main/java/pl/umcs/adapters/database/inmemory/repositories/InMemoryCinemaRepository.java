package pl.umcs.adapters.database.inmemory.repositories;

import pl.umcs.adapters.database.inmemory.InMemoryRepository;
import pl.umcs.domain.Cinema;
import pl.umcs.domain.repository.CinemaRepository;

public class InMemoryCinemaRepository extends InMemoryRepository<Cinema> implements CinemaRepository {
}
