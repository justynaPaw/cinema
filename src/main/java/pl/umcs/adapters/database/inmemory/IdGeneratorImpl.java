package pl.umcs.adapters.database.inmemory;

import pl.umcs.domain.repository.IdGenerator;

import java.util.HashMap;
import java.util.Map;

public class IdGeneratorImpl implements IdGenerator {

    Map<Class, Long> latestFreeIds = new HashMap<Class, Long>();

    @Override
    public long generateIdFor(Class clazz) {
        long id;
        if(latestFreeIds.containsKey(clazz)) {
            id = latestFreeIds.get(clazz);
        }
        else {
            id = 0;
        }
        latestFreeIds.put(clazz, id + 1);
        return id;
    }
}
