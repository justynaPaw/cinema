package pl.umcs.adapters.spring;

import org.springframework.web.bind.annotation.*;
import pl.umcs.commons.interfaces.CommandGateway;
import pl.umcs.domain.commands.CreateCinemaCommand;

@RestController
@RequestMapping("/cinema")
public class CinemaController {

    private CommandGateway commandGateway;

    public CinemaController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PutMapping("/new")
    public Long createNewCinema(@RequestBody CreateCinemaCommand command){
        return commandGateway.execute(command);
    }
}
