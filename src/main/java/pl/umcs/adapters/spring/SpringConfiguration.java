package pl.umcs.adapters.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.umcs.adapters.database.inmemory.IdGeneratorImpl;
import pl.umcs.adapters.database.inmemory.repositories.*;
import pl.umcs.commons.implementation.CommandGatewayImpl;
import pl.umcs.commons.implementation.HandlerRegistryImpl;
import pl.umcs.commons.interfaces.CommandGateway;
import pl.umcs.commons.interfaces.HandlerRegistry;
import pl.umcs.domain.repository.*;

import javax.validation.Validator;

@Configuration
public class SpringConfiguration {

    @Bean
    public HandlerRegistry getHandlerRegistry(CinemaRepository cinemaRepository, IdGenerator idGenerator, MovieRepository movieRepository, SeanceRepository seanceRepository,
                                              ReservationRepository reservationRepository, ClientRepository clientRepository) {
        return new HandlerRegistryImpl(idGenerator, cinemaRepository, movieRepository, seanceRepository, reservationRepository, clientRepository);
    }

    @Bean
    public CommandGateway getCommandGateway(HandlerRegistry handlerRegistry, Validator validator){
        return new CommandGatewayImpl(handlerRegistry, validator);
    }

    @Bean
    public IdGenerator getIdGenerator(){
        return new IdGeneratorImpl();
    }

    //repositories
    @Bean
    public CinemaRepository getCinemaRepository() {return new InMemoryCinemaRepository();}
    @Bean
    public MovieRepository getMovieRepository() {return new InMemoryMovieRepository();}
    @Bean
    public ClientRepository getClientRepository() {return new InMemoryClientRepository();}
    @Bean
    public SeanceRepository getSeanceRepository() {return new InMemorySeanceRepository();}
    @Bean
    public ReservationRepository getReservationRepository() {return new InMemoryReservationRepository();}

}
