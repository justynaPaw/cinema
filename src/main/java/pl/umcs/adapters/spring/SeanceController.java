package pl.umcs.adapters.spring;

import org.springframework.web.bind.annotation.*;
import pl.umcs.commons.interfaces.CommandGateway;
import pl.umcs.domain.Seat;
import pl.umcs.domain.commands.CreateSeanceCommand;
import pl.umcs.domain.commands.ShowFreeSeatsCommand;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/seance")
public class SeanceController {
    private CommandGateway commandGateway;

    public SeanceController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PutMapping("/new")
    public Long createNewSeance(@RequestBody CreateSeanceCommand command){
        return commandGateway.execute(command);
    }

    @PostMapping("/freeSeats")
    public List<Seat> getFreeSeats(@RequestBody ShowFreeSeatsCommand command){
        return commandGateway.execute(command);
    }
}
