package pl.umcs.adapters.spring;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.umcs.commons.interfaces.Command;
import pl.umcs.commons.interfaces.CommandGateway;
import pl.umcs.domain.commands.CreateMovieCommand;

@RestController
@RequestMapping("/movie")
public class MovieController {

    private CommandGateway commandGateway;

    public MovieController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PutMapping("/new")
    //@PreAuthorize("hasRole('USER')")
    public Long createNewMovie(@RequestBody CreateMovieCommand command){
        return commandGateway.execute(command);
    }
}
