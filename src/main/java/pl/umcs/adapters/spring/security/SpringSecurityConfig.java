package pl.umcs.adapters.spring.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    SimpleUrlAuthenticationSuccessHandlerBean successHandler;
    @Autowired
    SimpleUrlAuthenticationFailureHandlerBean failureBean;

    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/v2/api-docs", "/swagger-resources/**", "/swagger-ui.html", "/webjars/**").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/logout").authenticated()
                .antMatchers("/cinema/new").hasRole(UserRoles.ADMIN)
                .antMatchers("/movie/new").hasRole(UserRoles.ADMIN)
                .antMatchers("/seance/new").hasRole(UserRoles.ADMIN)
                .antMatchers("/reservation/newPaid").hasRole(UserRoles.CASHIER)
                .antMatchers("/reservation/markPaid").hasRole(UserRoles.CASHIER)
                .anyRequest().permitAll()
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(new AuthenticationEntryPoint() {
                    @Override
                    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
                        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You are unauthorized.");
                    }
                })
                .and()
                .logout()
                .logoutUrl("/logout")
                .and()
                .formLogin()
                .loginProcessingUrl("/login")
                .and()
                .csrf().disable()
        ;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user").password("{noop}user").roles(UserRoles.USER)
                .and()
                .withUser("admin").password("{noop}admin").roles(UserRoles.ADMIN)
                .and()
                .withUser("a").password("{noop}a").roles(UserRoles.ADMIN, UserRoles.CASHIER, UserRoles.USER)
                .and()
                .withUser("cashier").password("{noop}cashier").roles(UserRoles.CASHIER)
        ;
    }

    public static class UserRoles{
        public static final String USER = "USER";
        public static final String ADMIN = "ADMIN";
        public static final String CASHIER = "CASHIER";
    }
}
