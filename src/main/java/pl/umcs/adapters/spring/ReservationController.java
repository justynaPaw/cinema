package pl.umcs.adapters.spring;


import org.springframework.web.bind.annotation.*;
import pl.umcs.commons.interfaces.CommandGateway;
import pl.umcs.domain.commands.CancelReservationCommand;
import pl.umcs.domain.commands.CreateNewReservationCommand;
import pl.umcs.domain.commands.CreatePaidReservationCommand;
import pl.umcs.domain.commands.SetReservationPaidCommand;

@RestController
@RequestMapping("/reservation")
public class ReservationController {
    private CommandGateway commandGateway;

    public ReservationController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PutMapping("/new")
    public Long createNewReservation(@RequestBody CreateNewReservationCommand command){
        return commandGateway.execute(command);
    }

    @PostMapping("/newPaid")
    public Long createPaidReservation(@RequestBody CreatePaidReservationCommand command){
        return commandGateway.execute(command);
    }

    @PostMapping("/cancel")
    public boolean cancelReservation(@RequestBody CancelReservationCommand command){
        return commandGateway.execute(command);
    }

    @PostMapping("/markPaid")
    public boolean markReservationAsPaid(@RequestBody SetReservationPaidCommand command){
        return commandGateway.execute(command);
    }
}
