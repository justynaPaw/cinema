package pl.umcs.adapters.spring;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.umcs.domain.*;
import pl.umcs.domain.repository.*;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/database")
public class DataAdministratorController {

    private CinemaRepository cinemaRepository;
    private MovieRepository movieRepository;
    private ClientRepository clientRepository;
    private SeanceRepository seanceRepository;
    private ReservationRepository reservationRepository;

    public DataAdministratorController(CinemaRepository cinemaRepository,
                                       MovieRepository movieRepository,
                                       ClientRepository clientRepository,
                                       SeanceRepository seanceRepository,
                                       ReservationRepository reservationRepository) {
        this.cinemaRepository = cinemaRepository;
        this.movieRepository = movieRepository;
        this.clientRepository = clientRepository;
        this.seanceRepository = seanceRepository;
        this.reservationRepository = reservationRepository;
    }

    @GetMapping("/cinemas")
    public List<Cinema> getAllCinemas(){
        return cinemaRepository.getAll().stream().collect(Collectors.toList());
    }

    @GetMapping("/movies")
    public Collection<Movie> getAllMovies(){
        return movieRepository.getAll();
    }

    @GetMapping("/clients")
    public Collection<Client> getAllClients(){
        return clientRepository.getAll();
    }

    @GetMapping("/seances")
    public Collection<Seance> getAllSeances(){
        return seanceRepository.getAll();
    }

    @GetMapping("/reservations")
    public Collection<Reservation> getAllReservations(){
        return reservationRepository.getAll();
    }
}
